<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Kreait\Firebase;

use Kreait\Firebase\Factory;

use Kreait\Firebase\ServiceAccount;

use Kreait\Firebase\Database;

class FirebaseController extends Controller

{

//

    public function index()
    {

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/laravelwithfirebase-6ae34-firebase-adminsdk-t9rdp-3e1319b872.json');

        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://laravelwithfirebase-6ae34.firebaseio.com/')
            ->create();

        $db = $firebase->getDatabase();


        $db->getReference('0020')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS014",
            "courseFee" => "7749",
            "courseId" => "0007",
            "courseName" => "BANKING-IBPS SBI RRB DH",
            "courseOffer" => "46",
            "paidAmt" => "1000",
            "receiptNo" => "0011",
            "regDate" => "15-05-2018",
            "remainAmt" => "6749",
            "stuMob" => "7353561957",
            "txnId" => "1526388964147"
        ]);
        $db->getReference('0019')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS014",
            "courseFee" => "7749",
            "courseId" => "0007",
            "courseName" => "BANKING-IBPS SBI RRB DH",
            "courseOffer" => "46",
            "paidAmt" => "1000",
            "receiptNo" => "0012",
            "regDate" => "15-05-2018",
            "remainAmt" => "6749",
            "stuMob" => "8217227640",
            "txnId" => "1526389169182"
        ]);
        $db->getReference('0018')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS014",
            "courseFee" => "7749",
            "courseId" => "0007",
            "courseName" => "BANKING-IBPS SBI RRB DH",
            "courseOffer" => "46",
            "receiptNo" => "0013",
            "regDate" => "15-05-2018",
            "remainAmt" => "6749",
            "stuMob" => "9742701008",
            "txnId" => "1526389340874"
        ]);
        $db->getReference('0017')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS014",
            "courseFee" => "7749",
            "courseId" => "0007",
            "courseName" => "BANKING-IBPS SBI RRB DH",
            "courseOffer" => "46",
            "paidAmt" => "1000",
            "receiptNo" => "0014",
            "regDate" => "15-05-2018",
            "remainAmt" => "6749",
            "stuMob" => "8884135804",
            "txnId" => "1526389683038"
        ]);
        $db->getReference('0016')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS014",
            "courseFee" => "7749",
            "courseId" => "0007",
            "courseName" => "BANKING-IBPS SBI RRB DH",
            "courseOffer" => "46",
            "paidAmt" => "1000",
            "receiptNo" => "0015",
            "regDate" => "15-05-2018",
            "remainAmt" => "6749",
            "stuMob" => "8722889765",
            "txnId" => "1526389845950"
        ]);
        $db->getReference('0015')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS014",
            "courseFee" => "7749",
            "courseId" => "0007",
            "courseName" => "BANKING-IBPS SBI RRB DH",
            "courseOffer" => "46",
            "paidAmt" => "1000",
            "receiptNo" => "0016",
            "regDate" => "15-05-2018",
            "remainAmt" => "6749",
            "stuMob" => "7829533595",
            "txnId" => "1526389994642"
        ]);
        $db->getReference('0014')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS004",
            "courseFee" => "8610",
            "courseId" => "0004",
            "courseName" => "BANKING-IBPS,SBI,RRB,LIC,IPPS",
            "courseOffer" => "40",
            "paidAmt" => "7400",
            "receiptNo" => "0017",
            "regDate" => "19-05-2018",
            "remainAmt" => "1210",
            "stuMob" => "9739653050",
            "txnId" => "1526706938513"
        ]);
        $db->getReference('0013')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS004",
            "courseFee" => "8610",
            "courseId" => "0004",
            "courseName" => "BANKING-IBPS,SBI,RRB,LIC,IPPS",
            "courseOffer" => "40",
            "paidAmt" => "7400",
            "receiptNo" => "0018",
            "regDate" => "19-05-2018",
            "remainAmt" => "1210",
            "stuMob" => "7892193143",
            "txnId" => "1526707123533"
        ]);
        $db->getReference('0012')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS004",
            "courseFee" => "8610",
            "courseId" => "0004",
            "courseName" => "BANKING-IBPS,SBI,RRB,LIC,IPPS",
            "courseOffer" => "40",
            "paidAmt" => "7400",
            "receiptNo" => "0019",
            "regDate" => "19-05-2018",
            "remainAmt" => "1210",
            "stuMob" => "9742504137",
            "txnId" => "1526707341712"
        ]);
        $db->getReference('0011')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS004",
            "courseFee" => "8610",
            "courseId" => "0004",
            "courseName" => "BANKING-IBPS,SBI,RRB,LIC,IPPS",
            "courseOffer" => "40",
            "paidAmt" => "7400",
            "receiptNo" => "0020",
            "regDate" => "19-05-2018",
            "remainAmt" => "1210",
            "stuMob" => "8553596202",
            "txnId" => "1526707483306"
        ]);


        $db->getReference('0010')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS014",
            "courseFee" => "7749",
            "courseId" => "0007",
            "courseName" => "BANKING-IBPS SBI RRB DH",
            "courseOffer" => "46",
            "paidAmt" => "1000",
            "receiptNo" => "0003",
            "regDate" => "09-05-2018",
            "remainAmt" => "6749",
            "stuMob" => "7996721502",
            "txnId" => "1525865103327"
        ]);
        $db->getReference('0009')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS014",
            "courseFee" => "7749",
            "courseId" => "0007",
            "courseName" => "BANKING-IBPS SBI RRB DH",
            "courseOffer" => "46",
            "paidAmt" => "1000",
            "receiptNo" => "0004",
            "regDate" => "09-05-2018",
            "remainAmt" => "6749",
            "stuMob" => "7353668569",
            "txnId" => "1525865338812"
        ]);
        $db->getReference('0008')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS014",
            "courseFee" => "7749",
            "courseId" => "0007",
            "courseName" => "BANKING-IBPS SBI RRB DH",
            "courseOffer" => "46",
            "paidAmt" => "1000",
            "receiptNo" => "0005",
            "regDate" => "09-05-2018",
            "remainAmt" => "6749",
            "stuMob" => "9743175979",
            "txnId" => "1525865474329"
        ]);
        $db->getReference('0007')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS014",
            "courseFee" => "7749",
            "courseId" => "0007",
            "courseName" => "BANKING-IBPS SBI RRB DH",
            "courseOffer" => "46",
            "paidAmt" => "1000",
            "receiptNo" => "0006",
            "regDate" => "09-05-2018",
            "remainAmt" => "6749",
            "stuMob" => "9482004615",
            "txnId" => "1525865595458"
        ]);
        $db->getReference('0006')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS014",
            "courseFee" => "7749",
            "courseId" => "0007",
            "courseName" => "BANKING-IBPS SBI RRB DH",
            "courseOffer" => "46",
            "paidAmt" => "1000",
            "receiptNo" => "0007",
            "regDate" => "09-05-2018",
            "remainAmt" => "6749",
            "stuMob" => "8095828595",
            "txnId" => "1525865946703"
        ]);
        $db->getReference('0005')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS014",
            "courseFee" => "7749",
            "courseId" => "0007",
            "courseName" => "BANKING-IBPS SBI RRB DH",
            "courseOffer" => "46",
            "paidAmt" => "1000",
            "receiptNo" => "0008",
            "regDate" => "09-05-2018",
            "remainAmt" => "6749",
            "stuMob" => "9483036133",
            "txnId" => "1525867002474"
        ]);
        $db->getReference('0004')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS014",
            "courseFee" => "7749",
            "courseId" => "0007",
            "courseName" => "BANKING-IBPS SBI RRB DH",
            "courseOffer" => "46",
            "paidAmt" => "1000",
            "receiptNo" => "0009",
            "regDate" => "15-05-2018",
            "remainAmt" => "6749",
            "stuMob" => "9743868506",
            "txnId" => "1526388018972"
        ]);
        $db->getReference('0003')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS014",
            "courseFee" => "7749",
            "courseId" => "0007",
            "courseName" => "BANKING-IBPS SBI RRB DH",
            "courseOffer" => "46",
            "paidAmt" => "1000",
            "receiptNo" => "0010",
            "regDate" => "15-05-2018",
            "remainAmt" => "6749",
            "stuMob" => "9972045840",
            "txnId" => "1526388793330"
        ]);


        $db->getReference('0002')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS014",
            "courseFee" => "7749",
            "courseId" => "0007",
            "courseName" => "BANKING-IBPS SBI RRB DH",
            "courseOffer" => "46",
            "paidAmt" => "1000",
            "receiptNo" => "0002",
            "regDate" => "09-05-2018",
            "remainAmt" => "6749",
            "stuMob" => "9538861975",
            "txnId" => "1525864879630"
        ]);

        $db->getReference('0001')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS014",
            "courseFee" => "7749",
            "courseId" => "0007",
            "courseName" => "BANKING-IBPS SBI RRB DH",
            "courseOffer" => "46",
            "paidAmt" => "1000",
            "receiptNo" => "0001",
            "regDate" => "09-05-2018",
            "remainAmt" => "6749",
            "stuMob" => "8970331658",
            "txnId" => "1525864701691"
        ]);

        $db->getReference('0000')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS014",
            "courseFee" => "8610",
            "courseId" => "0004",
            "courseName" => "BANKING-IBPS,SBI,RRB,LIC,IPPS",
            "courseOffer" => "40",
            "paidAmt" => "8610",
            "receiptNo" => "0000",
            "regDate" => "08-05-2018",
            "remainAmt" => "0",
            "stuMob" => "9590520000",
            "txnId" => "1525759131873"
        ]);


        echo '<h1>Data as inserted in Firebase</h1>';

    }

}

?>